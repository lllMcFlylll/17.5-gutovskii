﻿// 17.5 - gutovskii.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <math.h>

class Example
{
private:
    int a;

public:

    int GetA()
    {
        return a;
    }

    void SetA(int NewA)
    {
        int a = NewA;
    }
};

class Vector
{
private:
    int x=2;
    int y=3;
    int z=4;

public:
    
    int GetM()
    {
        return sqrt ((x+y+z)*2);
    }

    void SetY(int NewY)
    {
        int y = NewY;
    }
};


int main()
{
    Example temp, temp1;
    temp.SetA(5);
    temp1.SetA(10);
    std::cout << temp.GetA() << ' ' << temp1.GetA();

    Vector temp2;
    temp2.SetY(7);
    std::cout << temp2.GetM();
}

